import React, { useState, useEffect } from 'react'
import fetch from 'isomorphic-unfetch'

const Home = () => {

  function demo(){
    console.log("this was pressed !")
  }
  const [data, setData] = useState([])
  useEffect(() => {
    const getData = async () => {
      const response = await fetch('https://swapi.dev/api/films/')
      console.log("response",response)
      const data = await response.json()
      console.log("data",data)
      setData(data.results)
    }
    getData()
  })
  return (
    <main>
      <div className='top-container' style={{marginTop:'150px',display:'flex', flexDirection: 'column',textAlign: '-webkit-center'}}>
      <h1 style={{fontSize:'55px'}}> The Starwars films</h1>
      <ul style={{listStyle:'none', display:'flex', flexDirection: 'column', gap:'20px', marginLeft:'37vw'}}>
        {data.map(item => (
          <li  
          style={{cursor:'pointer',background:'#E6E6EA',width:'400px', height:'30px',border:'solid grey', borderRadius:'8px', fontWeight:'bold'}}
          key={item.name}
          >
          {item.title}
          </li>
        ))}
      </ul>
      </div>
    </main>
  )
}

export default Home
