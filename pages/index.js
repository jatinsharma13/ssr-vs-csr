import React from 'react'
import Link from 'next/link'
import Image from 'next/image'
import Client from '../images/client.png'
import Server from '../images/server.png'
export default () => {
  return (
  
    <main style={{ background: 'white', height:'100vh'}} 
    >
      <h1 style={{ display: 'flex', marginLeft: '20%',fontSize: '56px', fontFamily:'Helvetica'}}>Normal rendering (CSR) VS SSR on NextJS</h1>
      <p style={{  marginLeft: '30%',fontSize: '20px'}}>Here we're building the same example, with two different approaches: CSR AND SSR</p>
      <p style={{  marginLeft: '43%',fontSize: '15px'}}>A demo by Jatin Sharma</p>

      <div className='top-holder' style={{display: 'flex',gap: '0vw',padding: '50px'}}>

      <Link style={{fontSize:'30px', display:'flex'}} href="/csr">
        <div className='csr' style={{width:'30vw',height:'maxContent',border:'solid grey',borderRadius:'16px', marginLeft:'170px', background:'#F1EEED', boxShadow:'7px 7px 7px grey'}}>
            <p style={{marginLeft:'25%', marginTop:'15%'}}>Client-side rendering </p>
           <Image src={Client}
           width={100}
           height={100}
           style={{marginLeft:'40%'}}
           />
        </div>
         </Link>

         <Link style={{fontSize:'30px', display:'flex'}} href="/ssr">
        <div className='ssr' style={{width:'30vw',height:'maxContent',border:'solid grey',borderRadius:'16px', marginLeft:'170px', background:'#F1EEED', boxShadow:'7px 7px 7px grey'}}>
            <p style={{marginLeft:'25%', marginTop:'15%'}}> Server-side rendering</p>
            <Image src={Server}
           width={100}
           height={100}
           style={{marginLeft:'40%'}}
           />
        </div>
         </Link>
      </div>
    </main>
  )
}
