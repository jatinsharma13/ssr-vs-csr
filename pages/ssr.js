import React from 'react'
import fetch from 'isomorphic-unfetch'

export async function getServerSideProps () {
  const response = await fetch('https://swapi.dev/api/films/')
  console.log("response",response)
  const data = await response.json()
  console.log("data",data)
  return { props: { data: data.results } }
}

const Home = ({ data = []}) => {
  return (
    <main>
       <div className='top-container' style={{marginTop:'150px',display:'flex', flexDirection: 'column',textAlign: '-webkit-center'}}>
      <h1 style={{fontSize:'55px'}}>The Starwars films</h1>
      <ul style={{listStyle:'none', display:'flex', flexDirection: 'column', gap:'20px', marginLeft:'38vw'}}>
        {data.map(item => (
          <li style={{width:'400px',background:'#E6E6EA', height:'30px',border:'solid grey', borderRadius:'8px', fontWeight:'bold'}} key={item.name}>{item.title}</li>
        ))}
      </ul>
      </div>
    </main>
  )
}
export default Home
